
'use strict';

var TelegramBot = require('node-telegram-bot-api');

// TODO: Database

var bot = new TelegramBot(process.env.API_TOKEN, { polling: true });

var isDebug = process.env.debug ? true : false;
var me;
var db = new Object();


if (isDebug) {
    bot.onText(/\/deposit ([0-9]{1,})/, (msg, result) => {
        
    });
    bot.onText(/\/withdrawal ([0-9]{1,})/, (msg, result) => {
        
    });
    
}

bot.on("inline_query", function(msg){
    console.log("inline_query: id="+msg.id+" query="+msg.query+" offset="+msg.offset);
    
});

// Private Start

bot.onText(/\/(start|help)/, (msg, result) = >{
    // checkPrivate, helps
});

bot.onText(/\/(bal|balance)/, (msg, result) = >{
    // checkPrivate
});

bot.onText(/\/(create) ([0-9]{1,}) ([0-9]{1,})/, (msg, result) = >{
    // checkPrivate
});


// Private End


bot.getMe()
.then((ret) => {
    me = ret;
})